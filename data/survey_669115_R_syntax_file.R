data <- read.csv("survey_669115_R_data_file.csv", quote = "'\"", na.strings=c("", "\"\""), stringsAsFactors=FALSE, fileEncoding="UTF-8-BOM")


# LimeSurvey Field type: F
data[, 1] <- as.numeric(data[, 1])
attributes(data)$variable.labels[1] <- "id"
names(data)[1] <- "id"
# LimeSurvey Field type: DATETIME23.2
data[, 2] <- as.character(data[, 2])
attributes(data)$variable.labels[2] <- "submitdate"
names(data)[2] <- "submitdate"
# LimeSurvey Field type: F
data[, 3] <- as.numeric(data[, 3])
attributes(data)$variable.labels[3] <- "lastpage"
names(data)[3] <- "lastpage"
# LimeSurvey Field type: A
data[, 4] <- as.character(data[, 4])
attributes(data)$variable.labels[4] <- "startlanguage"
names(data)[4] <- "startlanguage"
# LimeSurvey Field type: A
data[, 5] <- as.character(data[, 5])
attributes(data)$variable.labels[5] <- "seed"
names(data)[5] <- "seed"
# LimeSurvey Field type: DATETIME23.2
data[, 6] <- as.character(data[, 6])
attributes(data)$variable.labels[6] <- "startdate"
names(data)[6] <- "startdate"
# LimeSurvey Field type: DATETIME23.2
data[, 7] <- as.character(data[, 7])
attributes(data)$variable.labels[7] <- "datestamp"
names(data)[7] <- "datestamp"
# LimeSurvey Field type: F
data[, 8] <- as.numeric(data[, 8])
attributes(data)$variable.labels[8] <- "[🍻 Alcohol] Oefenvraag  Stel je voor dat we zouden scoren hoe hallucinogeen deze drugs zijn. De instructie is als volgt:   	Bepaal welke drug het meest hallucinogeen is en voer daar een 5 in. 	Bepaal dan welke drug het minst hallucinogeen is en voer daar een 1 in. 	Bepaal dan hoe de andere drugs scoren met een 1, 2, 3, 4, of 5.   Als je bijvoorbeeld vindt dat koffie heel hallucinogeen is, geef je die een 5. Als je vindt dat cannabis het minst hallucinogeen is, geef je die een 1. Daarna de rest: stel dat je alcohol even hallucinogeen vindt als koffie, dan krijg die ook een 5. Cocaine en ecstasy vindt je even hallucinogeen: en die zitten er tussenin. Die geef je dan allebei een 3."
names(data)[8] <- "practise_alcohol"
# LimeSurvey Field type: F
data[, 9] <- as.numeric(data[, 9])
attributes(data)$variable.labels[9] <- "[🚬 Cannabis] Oefenvraag  Stel je voor dat we zouden scoren hoe hallucinogeen deze drugs zijn. De instructie is als volgt:   	Bepaal welke drug het meest hallucinogeen is en voer daar een 5 in. 	Bepaal dan welke drug het minst hallucinogeen is en voer daar een 1 in. 	Bepaal dan hoe de andere drugs scoren met een 1, 2, 3, 4, of 5.   Als je bijvoorbeeld vindt dat koffie heel hallucinogeen is, geef je die een 5. Als je vindt dat cannabis het minst hallucinogeen is, geef je die een 1. Daarna de rest: stel dat je alcohol even hallucinogeen vindt als koffie, dan krijg die ook een 5. Cocaine en ecstasy vindt je even hallucinogeen: en die zitten er tussenin. Die geef je dan allebei een 3."
names(data)[9] <- "practise_cannabis"
# LimeSurvey Field type: F
data[, 10] <- as.numeric(data[, 10])
attributes(data)$variable.labels[10] <- "[👃 Cocaine] Oefenvraag  Stel je voor dat we zouden scoren hoe hallucinogeen deze drugs zijn. De instructie is als volgt:   	Bepaal welke drug het meest hallucinogeen is en voer daar een 5 in. 	Bepaal dan welke drug het minst hallucinogeen is en voer daar een 1 in. 	Bepaal dan hoe de andere drugs scoren met een 1, 2, 3, 4, of 5.   Als je bijvoorbeeld vindt dat koffie heel hallucinogeen is, geef je die een 5. Als je vindt dat cannabis het minst hallucinogeen is, geef je die een 1. Daarna de rest: stel dat je alcohol even hallucinogeen vindt als koffie, dan krijg die ook een 5. Cocaine en ecstasy vindt je even hallucinogeen: en die zitten er tussenin. Die geef je dan allebei een 3."
names(data)[10] <- "practise_cocaine"
# LimeSurvey Field type: F
data[, 11] <- as.numeric(data[, 11])
attributes(data)$variable.labels[11] <- "[💊 Ecstasy] Oefenvraag  Stel je voor dat we zouden scoren hoe hallucinogeen deze drugs zijn. De instructie is als volgt:   	Bepaal welke drug het meest hallucinogeen is en voer daar een 5 in. 	Bepaal dan welke drug het minst hallucinogeen is en voer daar een 1 in. 	Bepaal dan hoe de andere drugs scoren met een 1, 2, 3, 4, of 5.   Als je bijvoorbeeld vindt dat koffie heel hallucinogeen is, geef je die een 5. Als je vindt dat cannabis het minst hallucinogeen is, geef je die een 1. Daarna de rest: stel dat je alcohol even hallucinogeen vindt als koffie, dan krijg die ook een 5. Cocaine en ecstasy vindt je even hallucinogeen: en die zitten er tussenin. Die geef je dan allebei een 3."
names(data)[11] <- "practise_ecstasy"
# LimeSurvey Field type: F
data[, 12] <- as.numeric(data[, 12])
attributes(data)$variable.labels[12] <- "[☕ Koffie] Oefenvraag  Stel je voor dat we zouden scoren hoe hallucinogeen deze drugs zijn. De instructie is als volgt:   	Bepaal welke drug het meest hallucinogeen is en voer daar een 5 in. 	Bepaal dan welke drug het minst hallucinogeen is en voer daar een 1 in. 	Bepaal dan hoe de andere drugs scoren met een 1, 2, 3, 4, of 5.   Als je bijvoorbeeld vindt dat koffie heel hallucinogeen is, geef je die een 5. Als je vindt dat cannabis het minst hallucinogeen is, geef je die een 1. Daarna de rest: stel dat je alcohol even hallucinogeen vindt als koffie, dan krijg die ook een 5. Cocaine en ecstasy vindt je even hallucinogeen: en die zitten er tussenin. Die geef je dan allebei een 3."
names(data)[12] <- "practise_coffee"
# LimeSurvey Field type: F
data[, 13] <- as.numeric(data[, 13])
attributes(data)$variable.labels[13] <- "[🍻 Alcohol] Schadelijkheid voor de gebruiker  Scoor hier de drugs op schadelijkheid voor de gebruiker.  Hiermee bedoelen we gezondheidsrisico\'s zoals lange-termijn schade of kans op acute incidenten, sociale risico\'s door bijvoorbeeld veranderd gedrag of lagere productiviteit, en risico\'s door ongelukken door gebruik, bijvoorbeeld in het verkeer.   	Bepaal welke drug het meest schadelijk is en voer daar een 5 in. 	Bepaal dan welke drug het minst schadelijk is en voer daar een 1 in. 	Bepaal dan hoe de andere drugs scoren met een 1, 2, 3, 4, of 5."
names(data)[13] <- "damageSelf_alcohol"
# LimeSurvey Field type: F
data[, 14] <- as.numeric(data[, 14])
attributes(data)$variable.labels[14] <- "[🚬 Cannabis] Schadelijkheid voor de gebruiker  Scoor hier de drugs op schadelijkheid voor de gebruiker.  Hiermee bedoelen we gezondheidsrisico\'s zoals lange-termijn schade of kans op acute incidenten, sociale risico\'s door bijvoorbeeld veranderd gedrag of lagere productiviteit, en risico\'s door ongelukken door gebruik, bijvoorbeeld in het verkeer.   	Bepaal welke drug het meest schadelijk is en voer daar een 5 in. 	Bepaal dan welke drug het minst schadelijk is en voer daar een 1 in. 	Bepaal dan hoe de andere drugs scoren met een 1, 2, 3, 4, of 5."
names(data)[14] <- "damageSelf_cannabis"
# LimeSurvey Field type: F
data[, 15] <- as.numeric(data[, 15])
attributes(data)$variable.labels[15] <- "[👃 Cocaine] Schadelijkheid voor de gebruiker  Scoor hier de drugs op schadelijkheid voor de gebruiker.  Hiermee bedoelen we gezondheidsrisico\'s zoals lange-termijn schade of kans op acute incidenten, sociale risico\'s door bijvoorbeeld veranderd gedrag of lagere productiviteit, en risico\'s door ongelukken door gebruik, bijvoorbeeld in het verkeer.   	Bepaal welke drug het meest schadelijk is en voer daar een 5 in. 	Bepaal dan welke drug het minst schadelijk is en voer daar een 1 in. 	Bepaal dan hoe de andere drugs scoren met een 1, 2, 3, 4, of 5."
names(data)[15] <- "damageSelf_cocaine"
# LimeSurvey Field type: F
data[, 16] <- as.numeric(data[, 16])
attributes(data)$variable.labels[16] <- "[💊 Ecstasy] Schadelijkheid voor de gebruiker  Scoor hier de drugs op schadelijkheid voor de gebruiker.  Hiermee bedoelen we gezondheidsrisico\'s zoals lange-termijn schade of kans op acute incidenten, sociale risico\'s door bijvoorbeeld veranderd gedrag of lagere productiviteit, en risico\'s door ongelukken door gebruik, bijvoorbeeld in het verkeer.   	Bepaal welke drug het meest schadelijk is en voer daar een 5 in. 	Bepaal dan welke drug het minst schadelijk is en voer daar een 1 in. 	Bepaal dan hoe de andere drugs scoren met een 1, 2, 3, 4, of 5."
names(data)[16] <- "damageSelf_ecstasy"
# LimeSurvey Field type: F
data[, 17] <- as.numeric(data[, 17])
attributes(data)$variable.labels[17] <- "[☕ Koffie] Schadelijkheid voor de gebruiker  Scoor hier de drugs op schadelijkheid voor de gebruiker.  Hiermee bedoelen we gezondheidsrisico\'s zoals lange-termijn schade of kans op acute incidenten, sociale risico\'s door bijvoorbeeld veranderd gedrag of lagere productiviteit, en risico\'s door ongelukken door gebruik, bijvoorbeeld in het verkeer.   	Bepaal welke drug het meest schadelijk is en voer daar een 5 in. 	Bepaal dan welke drug het minst schadelijk is en voer daar een 1 in. 	Bepaal dan hoe de andere drugs scoren met een 1, 2, 3, 4, of 5."
names(data)[17] <- "damageSelf_coffee"
# LimeSurvey Field type: F
data[, 18] <- as.numeric(data[, 18])
attributes(data)$variable.labels[18] <- "[🍻 Alcohol] Schadelijkheid voor anderen  Scoor hier de drugs op schadelijkheid voor anderen.  Hiermee bedoelen we gevolgen van bijvoorbeeld verhoogde agressie, zoals gevechten of huiselijk geweld, of ongelukken door gebruik, zoals in het verkeer of in en om het huis.   	Bepaal welke drug het meest schadelijk is en voer daar een 5 in. 	Bepaal dan welke drug het minst schadelijk is en voer daar een 1 in. 	Bepaal dan hoe de andere drugs scoren met een 1, 2, 3, 4, of 5."
names(data)[18] <- "damageOther_alcohol"
# LimeSurvey Field type: F
data[, 19] <- as.numeric(data[, 19])
attributes(data)$variable.labels[19] <- "[🚬 Cannabis] Schadelijkheid voor anderen  Scoor hier de drugs op schadelijkheid voor anderen.  Hiermee bedoelen we gevolgen van bijvoorbeeld verhoogde agressie, zoals gevechten of huiselijk geweld, of ongelukken door gebruik, zoals in het verkeer of in en om het huis.   	Bepaal welke drug het meest schadelijk is en voer daar een 5 in. 	Bepaal dan welke drug het minst schadelijk is en voer daar een 1 in. 	Bepaal dan hoe de andere drugs scoren met een 1, 2, 3, 4, of 5."
names(data)[19] <- "damageOther_cannabis"
# LimeSurvey Field type: F
data[, 20] <- as.numeric(data[, 20])
attributes(data)$variable.labels[20] <- "[👃 Cocaine] Schadelijkheid voor anderen  Scoor hier de drugs op schadelijkheid voor anderen.  Hiermee bedoelen we gevolgen van bijvoorbeeld verhoogde agressie, zoals gevechten of huiselijk geweld, of ongelukken door gebruik, zoals in het verkeer of in en om het huis.   	Bepaal welke drug het meest schadelijk is en voer daar een 5 in. 	Bepaal dan welke drug het minst schadelijk is en voer daar een 1 in. 	Bepaal dan hoe de andere drugs scoren met een 1, 2, 3, 4, of 5."
names(data)[20] <- "damageOther_cocaine"
# LimeSurvey Field type: F
data[, 21] <- as.numeric(data[, 21])
attributes(data)$variable.labels[21] <- "[💊 Ecstasy] Schadelijkheid voor anderen  Scoor hier de drugs op schadelijkheid voor anderen.  Hiermee bedoelen we gevolgen van bijvoorbeeld verhoogde agressie, zoals gevechten of huiselijk geweld, of ongelukken door gebruik, zoals in het verkeer of in en om het huis.   	Bepaal welke drug het meest schadelijk is en voer daar een 5 in. 	Bepaal dan welke drug het minst schadelijk is en voer daar een 1 in. 	Bepaal dan hoe de andere drugs scoren met een 1, 2, 3, 4, of 5."
names(data)[21] <- "damageOther_ecstasy"
# LimeSurvey Field type: F
data[, 22] <- as.numeric(data[, 22])
attributes(data)$variable.labels[22] <- "[☕ Koffie] Schadelijkheid voor anderen  Scoor hier de drugs op schadelijkheid voor anderen.  Hiermee bedoelen we gevolgen van bijvoorbeeld verhoogde agressie, zoals gevechten of huiselijk geweld, of ongelukken door gebruik, zoals in het verkeer of in en om het huis.   	Bepaal welke drug het meest schadelijk is en voer daar een 5 in. 	Bepaal dan welke drug het minst schadelijk is en voer daar een 1 in. 	Bepaal dan hoe de andere drugs scoren met een 1, 2, 3, 4, of 5."
names(data)[22] <- "damageOther_coffee"
# LimeSurvey Field type: F
data[, 23] <- as.numeric(data[, 23])
attributes(data)$variable.labels[23] <- "[🍻 Alcohol] Voordelen van gebruik  Scoor hier de drugs op voordelen van gebruik.  Hiermee bedoelen we positieve effecten van gebruik, bijvoorbeeld sociale effecten, positieve gezondheidseffecten, of therapeutische toepassingen.   	Bepaal welke drug de meeste voordelen heeft en voer daar een 5 in. 	Bepaal dan welke drug de minste voordelen heeft en voer daar een 1 in. 	Bepaal dan hoe de andere drugs scoren met een 1, 2, 3, 4, of 5."
names(data)[23] <- "benefits_alcohol"
# LimeSurvey Field type: F
data[, 24] <- as.numeric(data[, 24])
attributes(data)$variable.labels[24] <- "[🚬 Cannabis] Voordelen van gebruik  Scoor hier de drugs op voordelen van gebruik.  Hiermee bedoelen we positieve effecten van gebruik, bijvoorbeeld sociale effecten, positieve gezondheidseffecten, of therapeutische toepassingen.   	Bepaal welke drug de meeste voordelen heeft en voer daar een 5 in. 	Bepaal dan welke drug de minste voordelen heeft en voer daar een 1 in. 	Bepaal dan hoe de andere drugs scoren met een 1, 2, 3, 4, of 5."
names(data)[24] <- "benefits_cannabis"
# LimeSurvey Field type: F
data[, 25] <- as.numeric(data[, 25])
attributes(data)$variable.labels[25] <- "[👃 Cocaine] Voordelen van gebruik  Scoor hier de drugs op voordelen van gebruik.  Hiermee bedoelen we positieve effecten van gebruik, bijvoorbeeld sociale effecten, positieve gezondheidseffecten, of therapeutische toepassingen.   	Bepaal welke drug de meeste voordelen heeft en voer daar een 5 in. 	Bepaal dan welke drug de minste voordelen heeft en voer daar een 1 in. 	Bepaal dan hoe de andere drugs scoren met een 1, 2, 3, 4, of 5."
names(data)[25] <- "benefits_cocaine"
# LimeSurvey Field type: F
data[, 26] <- as.numeric(data[, 26])
attributes(data)$variable.labels[26] <- "[💊 Ecstasy] Voordelen van gebruik  Scoor hier de drugs op voordelen van gebruik.  Hiermee bedoelen we positieve effecten van gebruik, bijvoorbeeld sociale effecten, positieve gezondheidseffecten, of therapeutische toepassingen.   	Bepaal welke drug de meeste voordelen heeft en voer daar een 5 in. 	Bepaal dan welke drug de minste voordelen heeft en voer daar een 1 in. 	Bepaal dan hoe de andere drugs scoren met een 1, 2, 3, 4, of 5."
names(data)[26] <- "benefits_ecstasy"
# LimeSurvey Field type: F
data[, 27] <- as.numeric(data[, 27])
attributes(data)$variable.labels[27] <- "[☕ Koffie] Voordelen van gebruik  Scoor hier de drugs op voordelen van gebruik.  Hiermee bedoelen we positieve effecten van gebruik, bijvoorbeeld sociale effecten, positieve gezondheidseffecten, of therapeutische toepassingen.   	Bepaal welke drug de meeste voordelen heeft en voer daar een 5 in. 	Bepaal dan welke drug de minste voordelen heeft en voer daar een 1 in. 	Bepaal dan hoe de andere drugs scoren met een 1, 2, 3, 4, of 5."
names(data)[27] <- "benefits_coffee"
# LimeSurvey Field type: F
data[, 28] <- as.numeric(data[, 28])
attributes(data)$variable.labels[28] <- "[🍻 Alcohol] Verslavingsrisico  Scoor hier de drugs op verslavingsrisico.  Hiermee bedoelen we hoe groot het risico is dat een gebruiker verslaafd raakt, en dat gaat dan zowel over psychofarmacologische verslaving als gedragsmatige verslaving.   	Bepaal welke drug het meest verslavend is en voer daar een 5 in. 	Bepaal dan welke drug het minst verslavend is en voer daar een 1 in. 	Bepaal dan hoe de andere drugs scoren met een 1, 2, 3, 4, of 5."
names(data)[28] <- "addiction_alcohol"
# LimeSurvey Field type: F
data[, 29] <- as.numeric(data[, 29])
attributes(data)$variable.labels[29] <- "[🚬 Cannabis] Verslavingsrisico  Scoor hier de drugs op verslavingsrisico.  Hiermee bedoelen we hoe groot het risico is dat een gebruiker verslaafd raakt, en dat gaat dan zowel over psychofarmacologische verslaving als gedragsmatige verslaving.   	Bepaal welke drug het meest verslavend is en voer daar een 5 in. 	Bepaal dan welke drug het minst verslavend is en voer daar een 1 in. 	Bepaal dan hoe de andere drugs scoren met een 1, 2, 3, 4, of 5."
names(data)[29] <- "addiction_cannabis"
# LimeSurvey Field type: F
data[, 30] <- as.numeric(data[, 30])
attributes(data)$variable.labels[30] <- "[👃 Cocaine] Verslavingsrisico  Scoor hier de drugs op verslavingsrisico.  Hiermee bedoelen we hoe groot het risico is dat een gebruiker verslaafd raakt, en dat gaat dan zowel over psychofarmacologische verslaving als gedragsmatige verslaving.   	Bepaal welke drug het meest verslavend is en voer daar een 5 in. 	Bepaal dan welke drug het minst verslavend is en voer daar een 1 in. 	Bepaal dan hoe de andere drugs scoren met een 1, 2, 3, 4, of 5."
names(data)[30] <- "addiction_cocaine"
# LimeSurvey Field type: F
data[, 31] <- as.numeric(data[, 31])
attributes(data)$variable.labels[31] <- "[💊 Ecstasy] Verslavingsrisico  Scoor hier de drugs op verslavingsrisico.  Hiermee bedoelen we hoe groot het risico is dat een gebruiker verslaafd raakt, en dat gaat dan zowel over psychofarmacologische verslaving als gedragsmatige verslaving.   	Bepaal welke drug het meest verslavend is en voer daar een 5 in. 	Bepaal dan welke drug het minst verslavend is en voer daar een 1 in. 	Bepaal dan hoe de andere drugs scoren met een 1, 2, 3, 4, of 5."
names(data)[31] <- "addiction_ecstasy"
# LimeSurvey Field type: F
data[, 32] <- as.numeric(data[, 32])
attributes(data)$variable.labels[32] <- "[☕ Koffie] Verslavingsrisico  Scoor hier de drugs op verslavingsrisico.  Hiermee bedoelen we hoe groot het risico is dat een gebruiker verslaafd raakt, en dat gaat dan zowel over psychofarmacologische verslaving als gedragsmatige verslaving.   	Bepaal welke drug het meest verslavend is en voer daar een 5 in. 	Bepaal dan welke drug het minst verslavend is en voer daar een 1 in. 	Bepaal dan hoe de andere drugs scoren met een 1, 2, 3, 4, of 5."
names(data)[32] <- "addiction_coffee"
# LimeSurvey Field type: F
data[, 33] <- as.numeric(data[, 33])
attributes(data)$variable.labels[33] <- "[Schadelijkheid voor de gebruiker] Weging van criteria ⚖️  Dit is de laatste. Je hebt nu drugs gescoord op vier criteria.  Het uiteindelijke doel is om te bepalen welke drugs in totaal, alles bij elkaar genomen, het slechtst zijn. Hiervoor worden je scores gecombineerd, en daarbij wordt elk criterium gewogen. Je kunt die gewichten hieronder aangeven.  Het laagste gewicht is 0: dan telt een criterium helemaal niet mee. Het hoogste gewicht is 10: dan telt het heel sterk mee.  Start met het toekennen van 10 aan het criterium dat het zwaarst moet tellen. Ken dan getallen toe aan de andere drie criteria."
names(data)[33] <- "weights_damageSelf"
# LimeSurvey Field type: F
data[, 34] <- as.numeric(data[, 34])
attributes(data)$variable.labels[34] <- "[Schadelijkheid voor anderen] Weging van criteria ⚖️  Dit is de laatste. Je hebt nu drugs gescoord op vier criteria.  Het uiteindelijke doel is om te bepalen welke drugs in totaal, alles bij elkaar genomen, het slechtst zijn. Hiervoor worden je scores gecombineerd, en daarbij wordt elk criterium gewogen. Je kunt die gewichten hieronder aangeven.  Het laagste gewicht is 0: dan telt een criterium helemaal niet mee. Het hoogste gewicht is 10: dan telt het heel sterk mee.  Start met het toekennen van 10 aan het criterium dat het zwaarst moet tellen. Ken dan getallen toe aan de andere drie criteria."
names(data)[34] <- "weights_damageOther"
# LimeSurvey Field type: F
data[, 35] <- as.numeric(data[, 35])
attributes(data)$variable.labels[35] <- "[Voordelen van gebruik] Weging van criteria ⚖️  Dit is de laatste. Je hebt nu drugs gescoord op vier criteria.  Het uiteindelijke doel is om te bepalen welke drugs in totaal, alles bij elkaar genomen, het slechtst zijn. Hiervoor worden je scores gecombineerd, en daarbij wordt elk criterium gewogen. Je kunt die gewichten hieronder aangeven.  Het laagste gewicht is 0: dan telt een criterium helemaal niet mee. Het hoogste gewicht is 10: dan telt het heel sterk mee.  Start met het toekennen van 10 aan het criterium dat het zwaarst moet tellen. Ken dan getallen toe aan de andere drie criteria."
names(data)[35] <- "weights_benefits"
# LimeSurvey Field type: F
data[, 36] <- as.numeric(data[, 36])
attributes(data)$variable.labels[36] <- "[Verslavingsrisico] Weging van criteria ⚖️  Dit is de laatste. Je hebt nu drugs gescoord op vier criteria.  Het uiteindelijke doel is om te bepalen welke drugs in totaal, alles bij elkaar genomen, het slechtst zijn. Hiervoor worden je scores gecombineerd, en daarbij wordt elk criterium gewogen. Je kunt die gewichten hieronder aangeven.  Het laagste gewicht is 0: dan telt een criterium helemaal niet mee. Het hoogste gewicht is 10: dan telt het heel sterk mee.  Start met het toekennen van 10 aan het criterium dat het zwaarst moet tellen. Ken dan getallen toe aan de andere drie criteria."
names(data)[36] <- "weights_addiction"
# LimeSurvey Field type: F
data[, 37] <- as.numeric(data[, 37])
attributes(data)$variable.labels[37] <- "[Professional in de preventie (bijvoorbeeld preventiewerker of campagneleider bij een verslavingszorginstelling, bij een GGD, etc)] Jouw achtergrond  Het kan interessant zijn om de scores te vergelijken tussen groepen mensen. Hieronder staat nog een optionele vraag die ons hierbij helpen. Je kunt deze ook leeg laten als je wil.  Ben je werkzaam in een van de volgende velden?  Geef voor elk veld ook aan hoeveel jaar ervaring je in dat domein hebt."
data[, 37] <- factor(data[, 37], levels=c(1,0),labels=c("Ja", "Niet geselecteerd"))
names(data)[37] <- "expertise_prevPrac"
# LimeSurvey Field type: A
data[, 38] <- as.character(data[, 38])
attributes(data)$variable.labels[38] <- "[Opmerking] [Professional in de preventie (bijvoorbeeld preventiewerker of campagneleider bij een verslavingszorginstelling, bij een GGD, etc)] Jouw achtergrond  Het kan interessant zijn om de scores te vergelijken tussen groepen mensen. Hieronder staat nog een optionele vraag die ons hierbij helpen. Je kunt deze ook leeg laten als je wil.  Ben je werkzaam in een van de volgende velden?  Geef voor elk veld ook aan hoeveel jaar ervaring je in dat domein hebt."
names(data)[38] <- "expertise_prevPraccomment"
# LimeSurvey Field type: F
data[, 39] <- as.numeric(data[, 39])
attributes(data)$variable.labels[39] <- "[Professional in de geneeskunde, verslavingszorg, of psychofarmacologie (arts, apotheker, etc)] Jouw achtergrond  Het kan interessant zijn om de scores te vergelijken tussen groepen mensen. Hieronder staat nog een optionele vraag die ons hierbij helpen. Je kunt deze ook leeg laten als je wil.  Ben je werkzaam in een van de volgende velden?  Geef voor elk veld ook aan hoeveel jaar ervaring je in dat domein hebt."
data[, 39] <- factor(data[, 39], levels=c(1,0),labels=c("Ja", "Niet geselecteerd"))
names(data)[39] <- "expertise_medPrac"
# LimeSurvey Field type: A
data[, 40] <- as.character(data[, 40])
attributes(data)$variable.labels[40] <- "[Opmerking] [Professional in de geneeskunde, verslavingszorg, of psychofarmacologie (arts, apotheker, etc)] Jouw achtergrond  Het kan interessant zijn om de scores te vergelijken tussen groepen mensen. Hieronder staat nog een optionele vraag die ons hierbij helpen. Je kunt deze ook leeg laten als je wil.  Ben je werkzaam in een van de volgende velden?  Geef voor elk veld ook aan hoeveel jaar ervaring je in dat domein hebt."
names(data)[40] <- "expertise_medPraccomment"
# LimeSurvey Field type: F
data[, 41] <- as.numeric(data[, 41])
attributes(data)$variable.labels[41] <- "[Professional in justitie (bijvoorbeeld politie, handhaving, Openbaar Ministerie, etc)] Jouw achtergrond  Het kan interessant zijn om de scores te vergelijken tussen groepen mensen. Hieronder staat nog een optionele vraag die ons hierbij helpen. Je kunt deze ook leeg laten als je wil.  Ben je werkzaam in een van de volgende velden?  Geef voor elk veld ook aan hoeveel jaar ervaring je in dat domein hebt."
data[, 41] <- factor(data[, 41], levels=c(1,0),labels=c("Ja", "Niet geselecteerd"))
names(data)[41] <- "expertise_police"
# LimeSurvey Field type: A
data[, 42] <- as.character(data[, 42])
attributes(data)$variable.labels[42] <- "[Opmerking] [Professional in justitie (bijvoorbeeld politie, handhaving, Openbaar Ministerie, etc)] Jouw achtergrond  Het kan interessant zijn om de scores te vergelijken tussen groepen mensen. Hieronder staat nog een optionele vraag die ons hierbij helpen. Je kunt deze ook leeg laten als je wil.  Ben je werkzaam in een van de volgende velden?  Geef voor elk veld ook aan hoeveel jaar ervaring je in dat domein hebt."
names(data)[42] <- "expertise_policecomment"
# LimeSurvey Field type: F
data[, 43] <- as.numeric(data[, 43])
attributes(data)$variable.labels[43] <- "[Ervaringsdeskundige (ervaren gebruiker van middelen, peer educator, etc)] Jouw achtergrond  Het kan interessant zijn om de scores te vergelijken tussen groepen mensen. Hieronder staat nog een optionele vraag die ons hierbij helpen. Je kunt deze ook leeg laten als je wil.  Ben je werkzaam in een van de volgende velden?  Geef voor elk veld ook aan hoeveel jaar ervaring je in dat domein hebt."
data[, 43] <- factor(data[, 43], levels=c(1,0),labels=c("Ja", "Niet geselecteerd"))
names(data)[43] <- "expertise_user"
# LimeSurvey Field type: A
data[, 44] <- as.character(data[, 44])
attributes(data)$variable.labels[44] <- "[Opmerking] [Ervaringsdeskundige (ervaren gebruiker van middelen, peer educator, etc)] Jouw achtergrond  Het kan interessant zijn om de scores te vergelijken tussen groepen mensen. Hieronder staat nog een optionele vraag die ons hierbij helpen. Je kunt deze ook leeg laten als je wil.  Ben je werkzaam in een van de volgende velden?  Geef voor elk veld ook aan hoeveel jaar ervaring je in dat domein hebt."
names(data)[44] <- "expertise_usercomment"
# LimeSurvey Field type: F
data[, 45] <- as.numeric(data[, 45])
attributes(data)$variable.labels[45] <- "[Wetenschapper in de geneeskunde of psychofarmacologie (bijvoorbeeld UD, UHD, of hoogleraar)] Jouw achtergrond  Het kan interessant zijn om de scores te vergelijken tussen groepen mensen. Hieronder staat nog een optionele vraag die ons hierbij helpen. Je kunt deze ook leeg laten als je wil.  Ben je werkzaam in een van de volgende velden?  Geef voor elk veld ook aan hoeveel jaar ervaring je in dat domein hebt."
data[, 45] <- factor(data[, 45], levels=c(1,0),labels=c("Ja", "Niet geselecteerd"))
names(data)[45] <- "expertise_medSci"
# LimeSurvey Field type: A
data[, 46] <- as.character(data[, 46])
attributes(data)$variable.labels[46] <- "[Opmerking] [Wetenschapper in de geneeskunde of psychofarmacologie (bijvoorbeeld UD, UHD, of hoogleraar)] Jouw achtergrond  Het kan interessant zijn om de scores te vergelijken tussen groepen mensen. Hieronder staat nog een optionele vraag die ons hierbij helpen. Je kunt deze ook leeg laten als je wil.  Ben je werkzaam in een van de volgende velden?  Geef voor elk veld ook aan hoeveel jaar ervaring je in dat domein hebt."
names(data)[46] <- "expertise_medScicomment"
# LimeSurvey Field type: F
data[, 47] <- as.numeric(data[, 47])
attributes(data)$variable.labels[47] <- "[Wetenschapper in de sociale wetenschappen (gedragsverandering, antropologie, etc)] Jouw achtergrond  Het kan interessant zijn om de scores te vergelijken tussen groepen mensen. Hieronder staat nog een optionele vraag die ons hierbij helpen. Je kunt deze ook leeg laten als je wil.  Ben je werkzaam in een van de volgende velden?  Geef voor elk veld ook aan hoeveel jaar ervaring je in dat domein hebt."
data[, 47] <- factor(data[, 47], levels=c(1,0),labels=c("Ja", "Niet geselecteerd"))
names(data)[47] <- "expertise_prevSci"
# LimeSurvey Field type: A
data[, 48] <- as.character(data[, 48])
attributes(data)$variable.labels[48] <- "[Opmerking] [Wetenschapper in de sociale wetenschappen (gedragsverandering, antropologie, etc)] Jouw achtergrond  Het kan interessant zijn om de scores te vergelijken tussen groepen mensen. Hieronder staat nog een optionele vraag die ons hierbij helpen. Je kunt deze ook leeg laten als je wil.  Ben je werkzaam in een van de volgende velden?  Geef voor elk veld ook aan hoeveel jaar ervaring je in dat domein hebt."
names(data)[48] <- "expertise_prevScicomment"
