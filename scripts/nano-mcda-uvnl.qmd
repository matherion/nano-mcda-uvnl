---
title: "Nano-MCDA Drugs 🍻🚬👃💊☕"
author: "Gjalt-Jorn Peters"
format:
  html:
    self-contained: true
    embed-resources: true
    code-fold: true
    code-tools: true    
editor: visual
editor_options: 
  chunk_output_type: console
---

This is the Quarto file containing the R chunks with the analyses for the Universiteit van Nederland movie available at <https://youtu.be/dlCGnS4AyOg>{.external target="_blank"}.

The OSF repo for this project is [https://osf.io/4ysbz](https://osf.io/4ysbz){.external target="_blank"}, and the git repo is <https://gitlab.com/matherion/nano-mcda-uvnl/>{.external target="_blank"}. The rendered version of this file is hosted at [https://matherion.gitlab.io/nano-mcda-uvnl/](https://matherion.gitlab.io/nano-mcda-uvnl/){.external target="_blank"}.

::: panel-tabset
## Setup

```{r}
#| label: setup

### "Slugs": parts of variable names used for easy selection

criteriaSlugs <- c("damageSelf", "damageOther",
                   "benefits", "addiction");
names(criteriaSlugs) <- criteriaSlugs;

drugSlugs <- c("alcohol", "cannabis", "cocaine",
               "ecstasy", "coffee");
names(drugSlugs) <- drugSlugs;

criteriaSlugs_recoded <- paste0(criteriaSlugs, "Recoded");
names(criteriaSlugs_recoded) <- criteriaSlugs_recoded;

drugSlugs_recoded <- paste0(drugSlugs, "Recoded");
names(drugSlugs_recoded) <- drugSlugs_recoded;

titles <-
  list(criterion =
         list(damageSelf = "Schade gebruiker",
              damageOther = "Schade anderen",
              benefits = "Voordelen",
              addiction = "Verslavingsrisico"),
       drug =
         list(
           alcohol = "Alcohol🍻",
           cannabis = "Cannabis🚬",
           cocaine = "Cocaine👃",
           ecstasy = "Ecstasy💊",
           coffee = "Koffie☕"
         )
  );

weightSigns <-
  list(
    damageSelf = 1,
    damageOther = 1,
    benefits = -1,
    addiction = 1
  );

```

### Function definitions

```{r}
#| label: function-definitions

ratingsToScores <- function(ratings,
                            weights,
                            weightOrder = names(weightSigns)) {
  return(mean(ratings * (unlist(weightSigns[weightOrder]) * weights)));
}

```

### Loading data

Here, we import the LimeSurvey data. Note that these data can be downloaded from the git repository at <https://gitlab.com/matherion/nano-mcda-uvnl/>{.external target="_blank"}.

```{r}
#| label: load-data

### Import data
dat.raw <-
  limonaid::ls_import_data(
    sid = 669115,
    path = here::here("data")
  );

```

### Clean data

Here, we clean the data.

```{r}
#| label: clean-data

### Delete unfinished entries
dat <-
  dat.raw[!is.na(dat.raw$submitdate), ];


### Rescale weights in case the highest weight was not 10
for (i in 1:nrow(dat)) {
  
  dat[i, paste0("weights_", names(titles$criterion))] <-
    as.numeric(trimws(dat[i, paste0("weights_", names(titles$criterion))]));
  
  multiplier <-
    10 / max(dat[i, paste0("weights_", names(titles$criterion))]);
  
  dat[i, paste0("correctedWeights_", names(titles$criterion))] <-
    multiplier * dat[i, paste0("weights_", names(titles$criterion))];

}

### Subtract 1 from every rating, so they run from 0-4
dat[, apply(expand.grid(criteriaSlugs_recoded, drugSlugs), 1, paste, collapse="_")] <-
  dat[, apply(expand.grid(criteriaSlugs, drugSlugs), 1, paste, collapse="_")] - 1;

### Multiply "benefits" ratings with -1 (they're not negative; "reverse damage")
dat[, gsub("benefits", "benefitsRecoded", grep("^benefits", names(dat), value=TRUE))] <-
  -1 * dat[, grepl("^benefits", names(dat))];

```

### Rating means

Here, we compute the means of the ratings.

```{r}
#| label: rating-means
#| fig-width: 6
#| fig-height: 4

ratingMeans <-
  unlist(
    lapply(
      criteriaSlugs_recoded,
      function(criterion) {
        return(
          lapply(
            drugSlugs,
            function(drug) {
              return(
                mean(
                  dat[, paste(criterion,
                              drug,
                              sep = "_")],
                  na.rm = TRUE
                )
              );
            }
          )
        );
      }
    ),
    recursive = FALSE
  );

ratingMeansDf <-
  data.frame(
    rating = unlist(ratingMeans)
  );

ratingMeansDf$criterion =
  factor(unlist(lapply(strsplit(names(ratingMeans), "\\."), `[[`, 1)),
         levels = unique(unlist(lapply(strsplit(names(ratingMeans), "\\."), `[[`, 1))),
         ordered = TRUE);

ratingMeansDf$drug =
  factor(unlist(lapply(strsplit(names(ratingMeans), "\\."), `[[`, 2)),
         levels = unique(unlist(lapply(strsplit(names(ratingMeans), "\\."), `[[`, 2))),
         ordered = TRUE);

### Also invert benefits ratings here
ratingMeansDf[ratingMeansDf$criterion == "benefits", "rating"] <-
  ratingMeansDf[ratingMeansDf$criterion == "benefits", "rating"] * -1;

```

### Aggregate scores

Here, we compute the aggregated scores.

```{r}
#| label: aggregate-scores
#| results: asis

### Weights

meanWeights <-
  colMeans(
    dat[, grep("correctedWeights_", names(dat), value=TRUE)]
  );

meanWeights_rescaled <- (10 / max(meanWeights)) * meanWeights;

names(meanWeights) <-
  names(titles$criterion);

### Add to rating means

ratingMeansDf$weight <-
  meanWeights_rescaled[ratingMeansDf$criterion];

ratingMeansDf$weightedRating <-
  ratingMeansDf$rating * ratingMeansDf$weight;

meanScores <- list();

for (drug in names(titles$drug)) {
  meanScores[[drug]] <-
    ratingsToScores(
      ratingMeansDf[ratingMeansDf$drug == drug, 'rating'],
      meanWeights_rescaled * 2,
      weightOrder = 1
    );
}

meanScoresDf <-
  data.frame(
    drug = names(titles$drug),
    score = unlist(meanScores)
  );

```

## Rating overview (ratings)

These sections show the frequencies of each rating for each drug and histograms visualizing the same information.

### Rating frequencies

```{r}
#| label: descriptives-rating-freqs

ratingFreqs <-
  unlist(
    lapply(
      criteriaSlugs_recoded,
      function(criterion) {
        return(
          lapply(
            drugSlugs,
            function(drug) {
              return(
                table(
                  dat[, paste(criterion,
                              drug,
                              sep = "_")]
                )
              );
            }
          )
        );
      }
    ),
    recursive = FALSE
  );

print(ratingFreqs);

```

### Rating histograms

```{r}
#| label: descriptives-rating-histograms
#| fig-width: 12
#| fig-height: 12

ratingPlots <-
  unlist(
    lapply(
      criteriaSlugs,
      function(criterion) {
        return(
          lapply(
            drugSlugs,
            function(drug) {
              res <-
                ggplot2::ggplot(
                  dat,
                  mapping = ggplot2::aes(
                    x = .data[[paste(criterion,
                                     drug,
                                     sep = "_")]]
                  )
                ) +
                  ggplot2::geom_bar(na.rm = TRUE) +
                  ggplot2::coord_cartesian(
                    xlim = c(1, 5),
                    ylim = c(1, max(unlist(ratingFreqs)))
                  ) +
                  ggplot2::scale_x_continuous(
                    breaks = 1:5
                  ) +
                  ggplot2::labs(
                    x = NULL,
                    y = NULL,
                    title = 
                      paste0(
                        titles$drug[[drug]],
                        ", ",
                        titles$criterion[[criterion]]
                      )
                  ) +
                  ggplot2::theme_minimal();
              if (criterion != criteriaSlugs[1]) {
                res <- res +
                  ggplot2::theme(
                    axis.title.y = ggplot2::element_blank(),
                    axis.text.y = ggplot2::element_blank(),
                    axis.ticks.y = ggplot2::element_blank()
                  );
              }
              if (drug != drugSlugs[5]) {
                res <- res +
                  ggplot2::theme(
                    axis.title.x = ggplot2::element_blank(),
                    axis.text.x = ggplot2::element_blank(),
                    axis.ticks.x = ggplot2::element_blank()
                  );
              }
              return(res);
            }
          )
        );
      }
    ),
    recursive = FALSE
  );

patchwork::wrap_plots(
  ratingPlots,
  byrow = FALSE,
  ncol = 4,
  guides = "collect"
);

```

## Rating overview (mean ratings)

This is an overview of the rating means as a line plot and as a bar plot.

### Rating means

```{r}
#| label: descriptives-rating-means
#| fig-width: 6
#| fig-height: 4

ratingMeansPlot <-
  ggplot2::ggplot(
    data = ratingMeansDf,
    mapping =
      ggplot2::aes(
        x = criterion,
        y = rating,
        group = drug,
        color = drug,
        fill = drug
      )
  ) +
  ggplot2::geom_line(linewidth = 1) +
  ggplot2::coord_cartesian(
    ylim = c(1, 5)
  ) +
  ggplot2::scale_y_continuous(
    breaks = 1:5
  ) +
  ggplot2::theme_minimal();

ratingMeansPlot;

```

### Aggregated scores

```{r}
#| label: aggregated-scores

knitr::kable(meanScoresDf);

ggplot2::ggplot(
  data = meanScoresDf,
  mapping = ggplot2::aes(
    x = drug,
    y = score,
    fill = drug
  )
) +
  ggplot2::geom_col() +
  ggplot2::scale_fill_viridis_d() +
  ggplot2::theme_minimal();

```

```{r}
#| label: aggregated-scores-pre-criterion

knitr::kable(ratingMeansDf);

ggplot2::ggplot(
  data = ratingMeansDf,
  mapping = ggplot2::aes(
    x = drug,
    y = weightedRating,
    fill = criterion,
    group = criterion
  )
) +
  ggplot2::geom_col() +
  ggplot2::scale_fill_viridis_d() +
  ggplot2::theme_minimal();

```

## Individual ratings

These are the ratings of every individual rater. This enables people to look up their own results, if they remember when they submitted those. Note that the time on the server isn't entirely accurate; I believe it was set 2 hours too early.

::: panel-tabset
```{r}
#| label: individual-ratings
#| results: asis

### Compute scores for each individual
for (drug in names(titles$drug)) {
  dat[, paste0("score_", drug)] <-
    apply(
      dat,
      1,
      function(dataRow) {
        return(
          ratingsToScores(
            ratings = as.numeric(trimws(dataRow[paste0(names(titles$criterion), "_", drug)])),
            weights = as.numeric(trimws(dataRow[paste0("correctedWeights_", names(titles$criterion))])) * 2
          )
        );
      }
    );
}

for (i in 1:nrow(dat)) {
  cat("\n\n###", dat[i, "submitdate"]);
  cat("\n\n#### Oefenvraag:\n");
  cat("\n- ", titles$drug$alcohol, ": ", dat[i, "practise_alcohol"], sep="");
  cat("\n- ", titles$drug$cannabis, ": ", dat[i, "practise_cannabis"], sep="");
  cat("\n- ", titles$drug$cocaine, ": ", dat[i, "practise_cocaine"], sep="");
  cat("\n- ", titles$drug$ecstasy, ": ", dat[i, "practise_ecstasy"], sep="");
  cat("\n- ", titles$drug$coffee, ": ", dat[i, "practise_coffee"], sep="");
  for (criterion in names(titles$criterion)) {
    cat("\n\n#### ", titles$criterion[[criterion]], ":\n", sep="");
    for (drug in names(titles$drug)) {
      cat("\n- ", titles$drug[[drug]], ": ",
          dat[i, paste0(criterion, "_", drug)], sep="");
    }
  }
  
  cat("\n\n#### Gewichten per criterium:\n", sep="");
  for (criterion in names(titles$criterion)) {
    cat("\n- ", titles$criterion[[criterion]], ": ",
        dat[i, paste0("correctedWeights_", criterion)],
        " (pre-rescaling: ",
        dat[i, paste0("weights_", criterion)],
        ")",
        sep="");
  }
  
  cat("\n\n#### Scores per drug:\n", sep="");
  for (drug in names(titles$drug)) {
    cat("\n- ", titles$drug[[drug]], ": ",
        dat[i, paste0("score_", drug)], sep="");
  }
  
}

```
:::

## Vragen en teksten

Hieronder staan de vragen en teksten zoals gebruikt om de data te verzamelen. Deze kunnen ook worden gedownload in PDF formaat, HTML formaat, of LimeSurvey `.lss` of `.txt` formaat van het [Open Science Framework](https://osf.io/4ysbz){.external target="_blank"} of [Git repository](https://gitlab.com/matherion/nano-mcda-uvnl/){.external target="_blank"}.

### Welkomstpagina

Welkom! 👋

We verzamelen met dit formulier input voor een aflevering van De Werkplaats van de Universiteit van Nederland.\* Het is een extreem versimpelde benadering van de inmiddels klassieke MCDA (Multi-Criterion Decision Analysis) van Nutt et al. (2010; doi.org/djcthv).

We vragen je om vijf drugs (🍻 alcohol, 🚬 cannabis, 👃 cocaine, 💊 ecstasy, en ☕ koffie) te beoordelen op vier criteria (of "uitkomsten"; schadelijkheid voor de gebruiker, schadelijkheid voor anderen, voordelen, en verslavingsrisico. Daarna vragen we je om die criteria te wegen. ⚖️

We leggen het verder uit op de volgende pagina's. We starten met een "oefenvraag".

Alvast bedankt voor het meedoen! 🙏

Gjalt-Jorn Peters

PS: deze survey is ontwikkeld om te gebruiken op kleine schermen (op telefoons). Als je 'm liever op je telefoon doet, dan kun je deze QR code scannen:

QR code die linkt naar naar https://ls3.ou.nl/669115

PPS: de uitkomsten worden uiteindelijk trouwens ook weer met je gedeeld op https://osf.io/4ysbz. 🔗

-   Dit is geen wetenschappelijke studie. De data worden verzameld om voorbeeld-data te hebben voor De Werkplaats van de Universiteit van Nederland. Omdat dit geen studie is, is er geen zogenaamd "informed consent". Er is ook geen onderzoeksvraag of hypothese, en deze data hebben dus niet vanzelfsprekend epistemische waarde.

### Oefenvraag

Stel je voor dat we zouden scoren hoe hallucinogeen deze drugs zijn. De instructie is als volgt:

-   Bepaal welke drug het *meest hallucinogeen* is en voer daar een 5 in.
-   Bepaal dan welke drug het *minst hallucinogeen* is en voer daar een 1 in.
-   Bepaal dan hoe de andere drugs scoren met een 1, 2, 3, 4, of 5.

**Antwoorden:**

- 🍻 Alcohol
- 🚬 Cannabis
- 👃 Cocaine
- 💊 Ecstasy
- ☕ Koffie 

Als je bijvoorbeeld vindt dat koffie heel hallucinogeen is, geef je die een 5. Als je vindt dat cannabis het minst hallucinogeen is, geef je die een 1. Daarna de rest: stel dat je alcohol even hallucinogeen vindt als koffie, dan krijg die ook een 5. Cocaine en ecstasy vindt je even hallucinogeen: en die zitten er tussenin. Die geef je dan allebei een 3.

ℹ️ Je hoeft deze oefenvraag niet echt in te vullen; je kunt gewoon op "volgende" klikken.

### Schadelijkheid voor de gebruiker

Scoor hier de drugs op **schadelijkheid voor de gebruiker**.

Hiermee bedoelen we gezondheidsrisico's zoals lange-termijn schade of kans op acute incidenten, sociale risico's door bijvoorbeeld veranderd gedrag of lagere productiviteit, en risico's door ongelukken door gebruik, bijvoorbeeld in het verkeer.

- Bepaal welke drug het *meest schadelijk* is en voer daar een 5 in.
- Bepaal dan welke drug het *minst schadelijk* is en voer daar een 1 in.
- Bepaal dan hoe de andere drugs scoren met een 1, 2, 3, 4, of 5.

**Antwoorden:**

- 🍻 Alcohol
- 🚬 Cannabis
- 👃 Cocaine
- 💊 Ecstasy
- ☕ Koffie 

ℹ️ Je moet minimaal één 5 hebben en één 1. Je mag dus 5, 5, 3, 1, 1 invoeren; of 1, 2, 3, 4, 5; of 1, 5, 1, 3, 3. Als je maar minstens een "schadelijkste drug" kiest (en 5 geeft) en minstens een "minst schadelijk drug" (die je 1 geeft) kun je verder de scores verdelen zoals je wil.


### Schadelijkheid voor anderen

Scoor hier de drugs op **schadelijkheid voor anderen**.

Hiermee bedoelen we gevolgen van bijvoorbeeld verhoogde agressie, zoals gevechten of huiselijk geweld, of ongelukken door gebruik, zoals in het verkeer of in en om het huis.

- Bepaal welke drug het *meest schadelijk* is en voer daar een 5 in.
- Bepaal dan welke drug het *minst schadelijk* is en voer daar een 1 in.
- Bepaal dan hoe de andere drugs scoren met een 1, 2, 3, 4, of 5.

**Antwoorden:**

- 🍻 Alcohol
- 🚬 Cannabis
- 👃 Cocaine
- 💊 Ecstasy
- ☕ Koffie 

ℹ️ Je moet minimaal één 5 hebben en één 1. Je mag dus 5, 5, 3, 1, 1 invoeren; of 1, 2, 3, 4, 5; of 1, 5, 1, 3, 3. Als je maar minstens een "schadelijkste drug" kiest (en 5 geeft) en minstens een "minst schadelijk drug" (die je 1 geeft) kun je verder de scores verdelen zoals je wil.

### Voordelen van gebruik

Scoor hier de drugs op **voordelen van gebruik**.

Hiermee bedoelen we positieve effecten van gebruik, bijvoorbeeld sociale effecten, positieve gezondheidseffecten, of therapeutische toepassingen.

- Bepaal welke drug de *meeste voordelen* heeft en voer daar een 5 in.
- Bepaal dan welke drug de *minste voordelen* heeft en voer daar een 1 in.
- Bepaal dan hoe de andere drugs scoren met een 1, 2, 3, 4, of 5.

**Antwoorden:**

- 🍻 Alcohol
- 🚬 Cannabis
- 👃 Cocaine
- 💊 Ecstasy
- ☕ Koffie 

ℹ️ Je moet minimaal één 5 hebben en één 1. Je mag dus 5, 5, 3, 1, 1 invoeren; of 1, 2, 3, 4, 5; of 1, 5, 1, 3, 3. Als je maar minstens een "drug met meeste voordelen" kiest (en 5 geeft) en minstens een "drug met minste voordelen" (die je 1 geeft) kun je verder de scores verdelen zoals je wil.

### Verslavingsrisico

Scoor hier de drugs op **verslavingsrisico**.

Hiermee bedoelen we hoe groot het risico is dat een gebruiker verslaafd raakt, en dat gaat dan zowel over psychofarmacologische verslaving als gedragsmatige verslaving.

- Bepaal welke drug het *meest verslavend* is en voer daar een 5 in.
- Bepaal dan welke drug het *minst verslavend* is en voer daar een 1 in.
- Bepaal dan hoe de andere drugs scoren met een 1, 2, 3, 4, of 5.

**Antwoorden:**

- 🍻 Alcohol
- 🚬 Cannabis
- 👃 Cocaine
- 💊 Ecstasy
- ☕ Koffie 

ℹ️ Je moet minimaal één 5 hebben en één 1. Je mag dus 5, 5, 3, 1, 1 invoeren; of 1, 2, 3, 4, 5; of 1, 5, 1, 3, 3. Als je maar minstens een "meest verslavende drug" kiest (en 5 geeft) en minstens een "minst verslavende drug" (die je 1 geeft) kun je verder de scores verdelen zoals je wil.

### Weging van criteria ⚖️

Dit is de laatste. Je hebt nu drugs gescoord op vier criteria.

Het uiteindelijke doel is om te bepalen welke drugs in totaal, alles bij elkaar genomen, het slechtst zijn. Hiervoor worden je scores gecombineerd, en daarbij wordt elk criterium gewogen. Je kunt die gewichten hieronder aangeven.

Het laagste gewicht is 0: dan telt een criterium helemaal niet mee. Het hoogste gewicht is 10: dan telt het heel sterk mee.

Start met het toekennen van 10 aan het criterium dat het zwaarst moet tellen. Ken dan getallen toe aan de andere drie criteria.

**Antwoorden:**

- Schadelijkheid voor de gebruiker
- Schadelijkheid voor anderen
- Voordelen van gebruik
- Verslavingsrisico

ℹ️ Als je alles even belangrijk vindt voor hoe slecht een drug in totaal is, zet dan alles op 10. Als je een criterium heel belangrijk vindt, de volgende de helft zo belangrijk, en de andere twee onbelangrijk, zet ze dan op 10, 5, 0, en 0.

De som moet minimaal 10 zijn (want een criterium moet 10 krijgen), en maximaal 40 (want je kunt hoogstens elk criterium 10 scoren).

### Jouw achtergrond

Het kan interessant zijn om de scores te vergelijken tussen groepen mensen. Hieronder staat nog een optionele vraag die ons hierbij helpen. Je kunt deze ook leeg laten als je wil.

Ben je werkzaam in een van de volgende velden?

Geef voor elk veld ook aan hoeveel jaar ervaring je in dat domein hebt.

- **Professional in de preventie** (bijvoorbeeld preventiewerker of campagneleider bij een verslavingszorginstelling, bij een GGD, etc)
- **Professional in de geneeskunde, verslavingszorg, of psychofarmacologie** (arts, apotheker, etc)
- **Professional in justitie** (bijvoorbeeld politie, Ministerie van Justitie en Veiligheid, handhaving, Openbaar Ministerie, advocatuur, etc)
- **Ervaringsdeskundige** (ervaren gebruiker van middelen, peer educator, etc)
- **Wetenschapper in de geneeskunde of psychofarmacologie** (bijvoorbeeld UD, UHD, of hoogleraar)
- **Wetenschapper in de sociale wetenschappen** (gedragsverandering, antropologie, etc)

### Afsluiting

Heel erg bedankt voor het meedoen!!! 🙏

Houd de site van de Universiteit van Nederland in de gaten - dit zou voor de zomer moeten verschijnen in De Werkplaats. 🛠️

Als we klaar zijn, plaatsen we de data in het Open Science Framework repository op https://osf.io/4ysbz. Je kunt die dus in de gaten houden mocht je willen kijken wat mensen zoals invulden. Houd er wel rekening mee dat deze data niet vanzelfsprekend ook epistemische waarde hebben.

Als je deze Nano-MCDA zou kunnen doorsturen aan andere mensen die in dit veld actief zijn, en die het leuk vinden om mee te doen, dan graag! De URL is https://ls3.ou.nl/669115, en de QR code is:

QR code die linkt naar naar https://ls3.ou.nl/669115
:::
