# Nano-MCDA UvNL

The final product is at https://youtu.be/dlCGnS4AyOg

The rendered page is at https://matherion.gitlab.io/nano-mcda-uvnl/

OSF repo: https://osf.io/4ysbz

See also https://youtu.be/Tb7Jp-4byBE for a previous, more extensive version.
